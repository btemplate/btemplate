Welcome To bTemplate
===================


bTemplate is a php HTML template class written by Brian Lozier brian@massassi.net  .

## How it Works

bTemplate is based on the assumption that the programmer shouldn't have to worry about coding a loop to assign variables to a block (or "section" in Smarty-speak). The idea behind bTemplate (and it's not an original one) is that the programmer simply populates an array and sets it once to the template. The template class is responsible for looping through that array, detecting any nested arrays (arbitrarily deep), and interpolating the values into the template automatically.

Let's go into a basic example. This example simply shows scalar tags, loops will be looked at shortly.

**simple.tpl:**
```

<html>
  <head>
    <title><tag:title /></title>
  </head>

  <body>
    <p>Hello, <tag:name /></p>
  </body>
</html> 
```
The corrosponding PHP page would look something like this. (There's no need to have a corrosponding PHP page for each template, that's just how the examples are being written.)

**simple.php:**
```
<?php
$title = 'My Simple Template Example';
$name = 'L.E. Modesitt, Jr.';

include_once('bTemplate.php');
$tpl = new bTemplate();

$tpl->set('title', $title);
$tpl->set('name', $name);

echo $tpl->fetch('simple.tpl');
?> 
```
Please note that there is no need to name the variable identical to the tag in the template. For example, $tpl->set('name', $honey_maid_grahams); is perfectly acceptable (easy to read is another matter).

The resulting outputted & interpolated template from the above two examples will look like this.

```
<html>
  <head>
    <title>My Simple Template Example</title>
  </head>

  <body>
    <p>Hello, L.E. Modsitt, Jr.</p>
  </body>
</html>
```

## Loops

Using the loop tag forces bTemplate to iterate over the elements of the assigned array.

**loop.php:**

```
<?php
$names = array('Tom', 'Dick, 'Harry');

include_once('bTemplate.php');
$tpl = new bTemplate();

$tpl->set('names', $names);

echo $tpl->fetch('loop.tpl');
?> 
```

**loop.tpl:**

```
<ul>
  <loop:names>
    <li><tag:names[] /></li>
  </loop:names>
</ul>
```

This simply prints out a bulleted list of all the names that were in the array. The [] after the tag name signifies that it's referencing a single element in a loop rather than a scalar $names variable. The resulting output would look like this.

```
<ul>
    <li>Tom</li>
    <li>Dick</li>
    <li>Harry</li>
</ul>
```
## Associative Arrays

Associative arrays are arrays that have strings as keys instead of integers. You can set an associative array just like you set a regular array or a scalar value.

**assoc.php:**

```
<?php
$record = array(
  'name' => 'Tom',
  'phone' => '555-1212',
  'zip' => '77777'
);

include_once('bTemplate.php');
$tpl = new bTemplate();

$tpl->set('record', $record);

echo $tpl->fetch('assoc.tpl');
?>
```

**assoc.tpl:**

```
<html>
  <head>
    <title>Associative Array Example</title>
  </head>

  <body>

  <table>
    <tr>
      <td>Name</td>
      <td>Phone</td>
      <td>Zip</td>
    </tr>
    <tr>
      <td><tag:record.name /></td>
      <td><tag:record.phone /></td>
      <td><tag:record.zip /></td>
    </tr>
  </table>

  </body>
</html> 
```

The result from this example would be like this.

```
<html>
  <head>
    <title>Associative Array Example</title>
  </head>

  <body>

  <table>
    <tr>
      <td>Name</td>
      <td>Phone</td>
      <td>Zip</td>
    </tr>
    <tr>
      <td>Tom</td>
      <td>555-1212</td>
      <td>77777</td>
    </tr>
  </table>

  </body>
</html> 
```

## Nested Loops

The final example will cover nested loops. It builds upon the previous loop code. A nested loop is simply an array element that is in itself another array. If you build arrays like this, you must code your template accordingly. I'll go over an example that simulates the results of a database query.


```
<loop:results>
  <ul>
  <li><tag:results[].name />
  <li><tag:results[].pass />
    <ul>
      <loop:results[].colors>
        <li><tag:results[].colors[] /></li>
      </loop:results[].colors>
    </ul>
  </ul>
</loop:results>
```

That may seem a bit confusing at first, but consider the structure of the array we will be assigning to the *results* tag.

```
$array = array(
  0 => array(
    'name' => 'Brian',
    'pass' => 'secret',
    'colors' => array(
      0 => 'red',
      1 => 'green',
      2 => 'blue'
    )
  ),
  1 => array(
    'name' => 'Mike',
    'pass' => 'freak',
    'colors' => array(
      0 => 'orange',
      1 => 'yellow',
      2 => 'black'
    )
  )
); 
```
The "base" array, if you will, is a numerically indexed array. Each index contains another array, this time an associative array. Further, each of the two associative arrays contain yet another numerically indexed array (called colors). bTemplate will loop through all of these, provided you have your template set up correctly. From the previous array and template, the resulting HTML will look like this.

```
<ul>
  <li>Brian
  <li>secret
  <ul>
    <li>red
    <li>green
    <li>blue
  </ul>
</ul>
<ul>
  <li>Mike
  <li>freak
  <ul>
    <li>orange
    <li>yellow
    <li>black
  </ul>
</ul> 
```


## Conditionals

Conditionals are simple "if" statements that allow you to conditonally display part of a template. If you have previously written an "is_logged_in()" function that returns TRUE or FALSE, you could use this in your template as follows.

**if.php:**

```
<?php
  $tpl->set('is_logged_in', is_logged_in(), TRUE);
?> 
```

Notice the third argument passed to the set() method. You must pass it exactly like that (with 'TRUE'). This third argument tells bTemplate to look for "if" statements referencing that variable.

**if.tpl:**

```
<if:is_logged_in>
  <p>Welcome, <tag:name /></p> 
<else:is_logged_in>
  You are not logged in.
</if:is_logged_in> 
```

Now, it will only attempt to print what's in the "if" statement if the is_logged_in() function returned TRUE.


## Case Loops

Case loops are a bit difficult to explain. They were designed such that you could loop through an array but take different actions based on a 'case' element of said array. This requires you to have a numerically indexed array with each element containing an associative array. This associative array will have whatever data you're looping through, as well as an option 'case' element. default is a reserved case and case is a reserved element name (so don't name any of your data elements 'case').

The steps are fairly simple. First you load up a $cases array (you can name the array anything you want). Then you load up your data array, assigning any element from the $cases array to a 'case' subscript. If you do not assign a case subscript, it will assume the 'default' case. Then you assign the data array and the cases array by using the set_cloop() method. It's not really as difficult as it sounds. Take a look at the code.

**case.php:**

```
<?php   $cases = array('color1', 'color2');
  $data[] = array('name' => 'George', 'case' => 'color1');
  $data[] = array('name' => 'Frodo', 'case' => 'color2');
  $data[] = array('name' => 'Freako', 'case' => 'color1');
  $data[] = array('name' => 'Whacked');

  $tpl->set_cloop('data', $data, $cases);
?> 
```

**case.tpl:**

```
<cloop:data>
  <case:color1>
    <p><font color="red"><tag:data[].name /></font></p>
  </case:color1>
  <case:color2>
    <p><font color="green"><tag:data[].name /></font></p>
  </case:color2>
  <case:default>
    <p><tag:data[].name /></p>
  </case:default>
</cloop:data>  
```
This loop will produce the following HTML output.

```
<p><font color="red">George</font></p>
<p><font color="green">Frodo</font></p>
<p><font color="red">Feako</font></p>
<p>Whacked</p> 
```

Case loops can also be used for other things. The most common uses are probably alternating table background colors and setting "selected" rows in a list box, some "checked" boxes in a series, and a "selected" radio button in a list.

## Master Templates

The idea of a "master template" is that you have the entire design of the site (minus the content) in one template file. This is different than most template systems in which you split the main design into a top and bottom, and then include both of those. This results in fragmented templates that can't be edited in most WYSIWYG editors (unless, of course, you feel like joining and splitting the two files each time you need to modify the design).

bTemplate doesn't specifically deal with this problem, but with a little bit of creativity, it's easy to overcome. First take a look at the master template.

**master.tpl:**

```
<html>
  <head>
    <title><tag:title /></title>
  </head>

  <body>
    <tag:content />
  </body>
</html> 
```

his is a very simple example of a master template, but remember, the template should contain every bit of your layout except dynamic features (logged in messages, status notification, form results, etc.) and the main content of the page. What we're doing is defining a "content area" that will be used to set the content through the PHP script.

This is handy if you get your data from a dynamic source such as a database. However, if you simply have a bunch of static pages and you're looking for an easier way to update the layout, you can also pull the data in from separate files with minimal HTML tags. Let's look at a couple of examples.

## Master Templates

While I don't recommend doing this (as it's obvious there is data mixed with HTML in this example), it's important to understand that you can parse the results of an external file (or even another template), and set it to a variable in another template (like the content variable we defined above).

**article.html:**
```
<h2>PHP Programming</h2>
<p>PHP is a recursive acronym for PHP: Hypertext Preprocessor. PHP is an interpreted language that is mostly used for developing web applications. Web applications are simply programs that run on a web server and are interfaced with by web clients (web browsers, in most cases).</p>  
```

If we want to take that article and insert it into our design, we simply use the fetch() method of bTemplate.

```
<?php
include_once('bTemplate.php');
$tpl = new bTemplate();

$title = 'My Article';
$tpl->set('title', $title);

$tpl->set('content', $tpl->fetch('article.html'));

echo $tpl->fetch('master.tpl');
?>
```
The resulting output would look like this.
```
<html>
  <head>
    <title>My Article</title>
  </head>

  <body>
<h2>PHP Programming</h2>
<p>PHP is a recursive acronym for PHP: Hypertext Preprocessor. PHP is an interpreted language that is mostly used for developing web applications. Web applications are simply programs that run on a web server and are interfaced with by web clients (web browsers, in most cases).</p>
  </body>
</html> 
```

## Populating From a Database

Let's say, for example, that the database table has a structure and data like this.

Table users:

| id 	| last_name 	| first_name 	| phone_number   	|
|----	|-----------	|------------	|----------------	|
| 1  	| Ness      	| Mike       	| (123) 555-1212 	|
| 2  	| Hetfield  	| James      	| (456) 555-1212 	|
| 3  	| Jovavich  	| Milla      	| (789) 555-1212 	|
| 4  	| Portman   	| Natalie    	| (012) 555-1212 	|

Our template for displaying such data would look like this. Please note that I'm intentionally leaving out the opening and closing HTML, HEAD, and BODY tags, as the idea behind this document is to show the use of master templates.

**users.tpl:**
```
<table border="1">
  <tr>
    <td><b>id</b></td>
    <td><b>last_name</b></td>
    <td><b>first_name</b></td>
    <td><b>phone_number</b></td>
  </tr>
  <loop:users>
    <tr>
      <td><tag:users[].id /></td>
      <td><tag:users[].last_name /></td>
      <td><tag:users[].first_name /></td>
      <td><tag:users[].phone_number /></td>
    </tr>
  </loop:users>
</table> 
```
The PHP script to populate the array and display the template should look something like this. Obviously you need to connect to the database and do all your normal error-checking routines.

```
<?php
// Assuming you've already connected to the database
$result = mysql_query('SELECT * FROM users');

// This loop populates an array with the data from the query
while($row = mysql_fetch_assoc($result)) {
  $users[] = $row;
}

include_once('bTemplate.php');
$tpl = new bTemplate();

$title = 'Database Results';
$tpl->set('title', $title);

// Now we set the array we populated
$tpl->set('users', $users);

// And here we fetch the users.tpl file and set it to content
$tpl->set('content', $tpl->fetch('users.tpl'));

echo $tpl->fetch('master.tpl');
?>  
```
The final output from this script should look like this.
```
<html>
  <head>
    <title>My Article</title>
  </head>

  <body>
<table border="1">
  <tr>
    <td><b>id</b></td>
    <td><b>last_name</b></td>
    <td><b>first_name</b></td>
    <td><b>phone_number</b></td>
  </tr>
    <tr>
      <td>0</td>
      <td>Ness</td>
      <td>Mike</td>
      <td>(123) 555-1212</td>
    </tr>
    <tr>
      <td>1</td>
      <td>Hetfield</td>
      <td>James</td>
      <td>(456) 555-1212</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Jovavich</td>
      <td>Milla</td>
      <td>(789) 555-1212</td>
    </tr>
    <tr>
      <td>3</td>
      <td>Portman</td>
      <td>Natalie</td>
      <td>(012) 555-1212</td>
    </tr>
</table>
  </body>
</html> 
```

